
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Login extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('user_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		

	}


	public function index() 
	{
		if ($this->session->userdata("login")) {
			redirect(base_url()."inicio");
		}else{
			$this->load->view('auth/login');
			}

	}


	public function login() {
		// load form helper and validation library
		
		if ($this->session->userdata("login")) {
			redirect(base_url()."inicio");
		}else{
			
			
		
		$data = new stdClass();
			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($this->user_model->resolve_user_login($username, $password)) {
				
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);
				
				$data = array ('id' => $user->id ,
			                    'nombre' => $user->username,
			                    'login'=> TRUE );
				$this->session->set_userdata($data);
				redirect(base_url()."inicio");
				
			} else {
				
				// login failed
				$data->error = 'Usuario o contrseña erroneos.';
				
				// send error to the view
				
				$this->load->view('auth/login', $data);
				
				
			}

		}
	}

	/**
	 * logout function.
	 *
	 * @access public
	 * @return void
	 */
	public function logout() {

		// create the data object
	#	$data = new stdClass();


			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}

			// user logout ok

			redirect('/');



	}

}
