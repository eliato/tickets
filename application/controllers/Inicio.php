
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Inicio extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		if (!$this->session->userdata("login")) {
			redirect(base_url());
		}

	}


	public function index() {
// user login ok
				$this->load->view('inicio/header');
				$this->load->view('inicio/inicio');
				$this->load->view('footer');
				

	}



	

}
